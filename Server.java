import java.util.Scanner;

public class Server {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int totalInput = scanner.nextInt();
        int totalTime = scanner.nextInt();
        int sumInput = 0;
        int countEligible = 0;
        for (int i = 0; i < totalInput; i++) {
            int inputTime = scanner.nextInt();
            sumInput += inputTime;
            if ((totalTime - sumInput) >= 0) {
                countEligible++;
            } else {
                break;
            }
        }
        System.out.println(countEligible);
    }

}
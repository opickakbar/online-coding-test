import java.util.Scanner;

public class AlmostPerfect {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        while (scanner.hasNextInt()) {
            int input = scanner.nextInt();
            System.out.println(input + " " + isPerfect(input));
        }
        scanner.close();
    }

    private static String isPerfect(int input) {
        double rootInput = Math.sqrt(input);
        int sumFactorNumbers = 1;
        if (input % rootInput == 0) {
            sumFactorNumbers += (int) rootInput;
        }
        int totalInputSubtractedBySumFactorNumbers;
        for (int i = 2; i < rootInput; i++) {
            if (input % i == 0) {
                sumFactorNumbers += (i + (input/i));
            }
        }
        totalInputSubtractedBySumFactorNumbers = Math.abs(input - sumFactorNumbers);
        if (totalInputSubtractedBySumFactorNumbers == 0) {
            return "perfect";
        }
        if (totalInputSubtractedBySumFactorNumbers <= 2) {
            return "almost perfect";
        }
        return "not perfect";
    }

}
